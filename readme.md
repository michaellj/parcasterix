# Technical test
**Summary**
- [Technical test](#technical-test)
	- [Technical informations](#technical-informations)
	- [Folders organisation](#folders-organisation)
	- [Usage](#usage)
		- [public folder (root)](#public-folder-root)
		- [src folder (root)](#src-folder-root)
		- [html folder (root)](#html-folder-root)
	- [Gulp tasks](#gulp-tasks)
			- [compile scss to css](#compile-scss-to-css)
			- [optimize css](#optimize-css)
			- [optimize js](#optimize-js)
			- [optimize images](#optimize-images)
			- [convert php files to html](#convert-php-files-to-html)
			- [create html production folder with files](#create-html-production-folder-with-files)
			- [watch file without php server](#watch-file-without-php-server)
	- [Export your work](#export-your-work)

## Technical informations

**Before you start coding in this repository you should check that everything you need is installed on your computer :**

| language | minimum version | link to documentation |
| -------- | --------------- | --------------------- |
| PHP      | 7.4.3           | [documentation](https://www.php.net/manual/fr/install.php) |
| Node.js  | 14.8.0          | [documentation](https://nodejs.org/en/) |
| Yarn     | 1.22.10         | [documentation](https://yarnpkg.com/getting-started/install) |
| Gulp.js  | 2.3.0           | [documentation](https://gulpjs.com/docs/en/getting-started/quick-start) |

___

## Folders organisation

Let's take a look at the basic directory's work tree :
```console
.[root folder]
|
|-- html
|	
|--	src
|    |-- datas
|    |    |-- pages.php
|    |
|    |-- partials
|    |    |-- foot.php
|    |    |-- head.php
|    |
|    |-- public
|    |    |-- css
|    |    |    |-- main.css
|    |    |    |-- main.css.map
|    |    |
|    |    |-- img
|    |    |    |-- backgrounds
|    |    |    |-- icons
|    |    |    |-- logos
|    |    |
|    |    |-- js
|    |    |    |-- main.js
|    |    |
|    |    |-- sass
|    |        |-- abstract
|    |        |    |-- __functions.scss
|    |        |    |-- __mixins.scss
|    |        |    |-- __variables.scss
|    |        |    |-- _abstract.scss
|    |        |
|    |        |-- base
|    |        |    |-- __reset.scss
|    |        |    |-- __typographies.scss
|    |        |    |-- _base_.scss
|    |        |
|    |        |-- components
|    |        |    |-- __access.scss
|    |        |    |-- __backgrounds.scss
|    |        |    |-- __booking.scss
|    |        |    |-- __button.scss
|    |        |    |-- __form.scss
|    |        |    |-- __header.scss
|    |        |    |-- __jumbo.scss
|    |        |    |-- __top-header.scss
|    |        |    |-- _components.scss
|    |        |
|    |        |-- layouts
|    |        |    |-- __container.scss
|    |        |    |-- __hero.scss
|    |        |    |-- _layouts.scss
|    |        |
|    |        |-- utils
|    |        |    |-- _utils.scss
|    |        |
|    |        |-- main.scss
|    |
|    |-- sections
|    |    |-- access.php
|    |    |-- booking_form.php
|    |    |-- header_scroll.php
|    |    |-- header.php
|    |    |-- top_header.php
|    |
|    |-- templates
|        |-- home.php
|
|-- gulpfile.js
|-- index.php
|-- package.json
|-- yarn.lock
```
___

## Usage
Right after pullin this repo you should run the following command :

```console
$ yarn install
```

Rune the gulp command to generate the html folder with all website files :
```console
$ gulp
```
It will automatically generate the `html` folder with all production compiled and optimized files.

>You should never touch the content included in the `html` folder but work directly in root `public` and `src` folders as your files will be compiled and optimized.
>
> `html` folder contains files for production.

Once your `html` folder is ready just type the following command in your terminal to watch every changes on your dev files
```console
$ yarn dev
```
Php server is now running on port 3000 and Gulp is watching every changement on your source files.<br/>
Open your favorite browser (I hope it's not IE) and you can see the result of your work at `localhost:3000`

### public folder (root)
Place your scss, css, js in these folders.<br/>
For javascripts files you can use ES6 syntaxe (*note that 'require' and 'import' does not actually work*).<br/>
For styling you should use scss files.

### src folder (root)
This folder is specific to PHP files. Use it to get or create reusable templates, use and add datas, use and create helpers functions

### html folder (root)
This folder will be automatically be generated and will contain the website templates with optimized versions of root `public` folder's files. and root `.php` files<br/>
**Don't touch it until you really need to !**

___

## Gulp tasks
Gulp tasks can be played individually in the terminal :
These command assume gulp cli is installed on your machine. If no, you will need at least npx to run a command.

So instead of
```console
$ gulp <command>
```
you should type
```console
$ npx gulp <command>
```

#### compile scss to css
```console
$ gulp scss
``` 
#### optimize css
```console
$ gulp css
``` 
#### optimize js
```console
$ gulp js
``` 
#### optimize images
```console
$ gulp img
``` 
#### convert php files to html
```console
$ gulp html
``` 
#### create html production folder with files
```console
$ gulp
```
#### watch file without php server
```console
$ gulp watch
```

___

## Export your work
If you need to export and share your template you can simply run this command :
```console
$ yarn build
```
It will generate a .zip file at the root of your directory with the name `website.zip` includind html, css, js, images files.

___

Thanks for reading.

Mika