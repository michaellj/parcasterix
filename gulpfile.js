// -------------------------------- //
// import libraries                 //
// -------------------------------- //

// common libraries
const { series, parallel, src, dest, watch } = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const replace = require("gulp-replace");
// const concat = require("gulp-concat"); // *uncomment this line to allow css and js files concatenation

// html libs
const php2html = require("gulp-php2html");
const beautify = require("gulp-beautify");
// const htmlMin = require("gulp-htmlmin"); // *uncomment file to allow html content compression

// css libs
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");

// js libs
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");

// img libs
const imagemin = require("gulp-imagemin");

// build libs
const zip = require("gulp-zip");

// -------------------------------- //
// file variables                   //
// -------------------------------- //
const sassSrc = "src/public/sass/**/*.scss";
const sassDist = "src/public/css";
const cssFileList = ["src/public/css/**/*.css"];
const cssSrc = cssFileList;
const cssDist = "html/public/css";
const jsFileList = ["src/public/js/**/*.js"];
const jsSrc = jsFileList;
const jsDist = "html/public/js";
const imgSrc = "src/public/img/**/*.{png,jpg,jpeg,pdf,gif,svg}";
const imgDist = "html/public/img";
const htmlSrc = "*.php";
const htmlDist = "html";
const buildSrc = "html/**/*";
const buildDist = ".";

// -------------------------------- //
// create gulp tasks                //
// -------------------------------- //
function scss() {
	return src(sassSrc)
		.pipe(sourcemaps.init())
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer({ cascade: false }))
		.pipe(sourcemaps.write("."))
		.pipe(dest(sassDist));
}

function css() {
	return src(cssSrc)
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(autoprefixer({ cascade: false }))
		.pipe(cleanCSS({ compatibility: "ie8" }))
		.pipe(sourcemaps.write("."))
		.pipe(dest(cssDist));
}

function js() {
	return src(jsSrc)
		.pipe(sourcemaps.init())
		.pipe(
			babel({
				presets: ["@babel/env"],
			})
		)
		.pipe(uglify({ ie8: true }))
		.pipe(sourcemaps.write("."))
		.pipe(dest(jsDist));
}

function img() {
	return src(imgSrc)
		.pipe(
			imagemin([
				imagemin.gifsicle({ interlaced: true }),
				imagemin.mozjpeg({ quality: 75, progressive: true }),
				imagemin.optipng({ optimizationLevel: 5 }),
				imagemin.svgo({
					plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
				}),
			])
		)
		.pipe(dest(imgDist));
}

function html() {
	return (
		src(htmlSrc)
			.pipe(php2html())
			.pipe(replace("./src/public/", "./public/"))
			.pipe(
				beautify.html({
					indent_size: 4,
					indent_with_tabs: true,
				})
			)
			// .pipe(htmlMin({ collapseWhitespace: true })) // *uncomment this line to compress html files content on one line
			.pipe(dest(htmlDist))
	);
}

function watchFiles() {
	watch(sassSrc, scss);
	watch(cssSrc, css);
	watch(jsSrc, js);
	watch(imgSrc, img);
	watch(htmlSrc, html);
}

function build() {
	return src(buildSrc).pipe(zip("website.zip")).pipe(dest(buildDist));
}

// -------------------------------- //
// making tasks available + runners //
// -------------------------------- //
exports.css = css;
exports.js = js;
exports.html = html;
exports.scss = scss;
exports.img = img;
exports.watchFiles = watchFiles;
exports.build = build;
exports.default = parallel(series(scss, css), js, img, html);
