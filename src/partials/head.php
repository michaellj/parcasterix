<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $page['title'] ?></title>
<meta name="description" content="<?php echo $page['description']; ?>">
<link rel="stylesheet" href="https://use.typekit.net/ned0gsi.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_green.css">
<link rel="stylesheet" href="./src/public/css/main.css">