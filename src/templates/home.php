<div class="home">
	<div class="hero u-block-flex--column u-flex-align--center">
		<?php
		require('src/sections/top_header.php');
		require('src/sections/header.php');
		?>

		<div class="jumbo u-text--center">
			<p class="jumbo__text jumbo__text--small">Plongez dans l'univers du parc Astérix</p>
			<p class="jumbo__text jumbo__text--large">Par toutatis !</p>
			<img src="./src/public/img/icons/chevron-down-white.png" class="icon">
		</div>

		<!-- START images composition -->
		<img id="background--decor" class="background-asset" src="./src/public/img/backgrounds/decor.png" />
		<img id="background--nuages02" class="background-asset" src="./src/public/img/backgrounds/nuages2.png" />
		<img id="background--nacelle" class="background-asset" src="./src/public/img/backgrounds/nacelle.png" />
		<img id="background--nuages" class="background-asset " src="./src/public/img/backgrounds/nuages.png" />
		<img id="background--oiseaux" class="background-asset " src="./src/public/img/backgrounds/oiseaux.png" />
		<!-- END images composition -->

		<div class="container u-block-flex--column u-flex-align--center">
			<?php
			require('src/sections/booking_form.php');
			require('src/sections/access.php');
			?>
		</div>
	</div>
</div>