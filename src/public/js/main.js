// -------------------------------
// functions
// -------------------------------

/**
 * @function handleBookingWithHotelSwitch
 * render input switch value when user click on the button
 * @param {String} inputID
 * the HTML input's id we should take value
 * @example
 * handleBookingWithHotelSwitch('elementID')
 */
function handleBookingWithHotelSwitch(inputID) {
	const elementID = `#${inputID}`;
	const bookingWithHotel = document.querySelector(elementID);
	console.log(bookingWithHotel.checked);
}

/**
 * @function flatPickerConfig
 * configure flatPicker for all listed inputs presents on a page
 * @param {Array.<String>} inputIDList
 * array of strings taking a list of html input id without hash syumbol
 * @example
 * flatPickerConfig(['idOne', 'idTwo'])
 */
function flatPickerConfig(inputIDList) {
	flatpickr.l10ns.default.firstDayOfWeek = 1;
	const inputList = inputIDList.map((id) => `#${id}`);

	inputList.forEach((id) => {
		if (document.querySelector(id)) {
			flatpickr(id, {
				locale: "fr",
				disableMobile: "true",
			});
		}
	});
}

/**
 * @function handleIncrement
 * Increment or decrement a value of DOM element
 * @param {String} elementID
 * The html id of the element to increment
 * @param {String} type
 * Should be 'increment' or 'decrement'
 * @param {String} destinationID
 * The html id of the element which will prompt the value to the screen
 */
function handleIncrement(elementID, type = "increment", destinationID) {
	let element = null;
	let value = null;
	if (document.querySelector(elementID)) {
		element = document.querySelector(elementID);
		value = parseInt(element.value);

		if (type === "increment") {
			element.value = value + 1;
		} else if (type === "decrement") {
			if (value > 0) {
				element.value = value - 1;
			} else {
				element.value = 0;
			}
		}

		if (destinationID) {
			setElementTextValue(destinationID, parseInt(element.value));
		}
	}
}

/**
 * @function setInputDefaultValue
 * @param {String} elementID
 * @param {Number} value
 */
function setInputDefaultValue(elementID, value) {
	let element = null;
	if (document.querySelector(elementID)) {
		element = document.querySelector(elementID);
		element.value = value;
	}
}

/**
 * @function setElementTextValue
 * @param {String} elementID
 * @param {String} value
 */
function setElementTextValue(elementID, value) {
	let element = null;
	if (document.querySelector(elementID)) {
		element = document.querySelector(elementID);
		element.innerText = value;
	}
}

/**
 * @function setHiddenInput
 * @param {String} elementID
 * @param {Number} value
 */
function setHiddenInput(elementID, value) {
	const textDestinationID = elementID + "Value";
	setElementTextValue(textDestinationID, value);
	setInputDefaultValue(elementID, value);
}

/**
 * @function setStickyHeader
 * allow you to use a secondary header which appears on scroll down
 * @param {String} headerID
 * html id of header element to work with
 */
function setStickyHeader(headerID) {
	if (document.querySelector(headerID)) {
		const header = document.querySelector(headerID);
		window.addEventListener("scroll", () => {
			if (window.scrollY > 150) {
				header.style.cssText = "position:fixed;top:0rem";
			} else {
				header.style.cssText = "position:absolute;top:-15rem";
			}
		});
	}
}

// -------------------------------
// run necessary scripts
// -------------------------------

setStickyHeader("#stickyHeader");
flatPickerConfig(["bookingCheckIn", "bookingCheckOut"]);
setHiddenInput("#bookingChildren", 0);
setHiddenInput("#bookingAdults", 0);
