<form action="/" method="POST" id="bookingForm" class="booking__module" onsubmit="event.preventDefault();">
	<label for="bookingWithHotel" class="booking__switch">
		<input type="checkbox" name="bookingWithHotel" id="bookingWithHotel">
		<span class="booking__switch-slider" onclick="handleBookingWithHotelSwitch('bookingWithHotel')"></span>
	</label>

	<div class="booking__input-section">
		<label for="" class="form__label booking__label">Dates</label>
		<div class="form__inline-section">
			<input id="bookingCheckIn" class="form__controller booking__input-inline" type="text" name="bookingCheckIn" placeholder="Jour arrivée" required />
			<input id="bookingCheckOut" class="form__controller boooking__input-inline u-text--center" type="text" name="bookingCheckOut" placeholder="Jour départ" required />
			<span class="icon__container">
				<img class="icon" src="./src/public/img/icons/arrow-right-white.png" />
			</span>
		</div>
	</div>

	<div class="booking__people-section">
		<div class="booking__input-section">
			<label for="" class="form__label booking__label">Enfants (3 à 11 ans)</label>
			<div class="form__inline-section">
				<button class="form__button--square" onclick="handleIncrement('#bookingChildren', 'decrement', '#bookingChildrenValue')">-</button>
				<input id="bookingChildren" class="form__controller--square booking__input-inline u-hide" type="number" name="bookingChildren" min="0" value="0" />
				<span class="form__controller--square" id="bookingChildrenValue"></span>
				<button class="form__button--square" onclick="handleIncrement('#bookingChildren', 'increment', '#bookingChildrenValue')">+</button>
			</div>
			<small><a href="#" class="form__small">PMR ?</a></small>
		</div>

		<div class="booking__input-section">
			<label for="" class="form__label booking__label">Adultes (+12 ans)</label>
			<div class="form__inline-section">
				<button class="form__button--square" onclick="handleIncrement('#bookingAdults', 'decrement', '#bookingAdultsValue')">-</button>
				<input id="bookingAdults" class="form__controller--square booking__input-inline u-hide" type="number" name="bookingAdults" min="0" value="0" />
				<span class="form__controller--square" id="bookingAdultsValue"></span>
				<button class="form__button--square" onclick="handleIncrement('#bookingAdults', 'increment', '#bookingAdultsValue')">+</button>
			</div>
			<small><a href="#" class="form__small">Plus de 20 personnes</a></small>
		</div>
	</div>

	<button class="btn btn-primary" type="submit">Réserver</button>
</form>