<div class="access">
	<p class="access__title">Accès rapides :</p>
	<ul class="access__list">
		<li class="access__item">
			<a href="#" class="access__link">
				<img srcset="
					./src/public/img/icons/horaires.png 12w,
					./src/public/img/icons/horaires-big.png 30w
					" sizes="
					(max-width: 980px) 30px,
					12px
					" src="./src/public/img/icons/horaires.png" alt="Horaires du parc" class="access__icon" />
				Horaires et calendriers
			</a>
		</li>

		<li class="access__item">
			<a href="#" class="access__link">
				<img srcset="
					./src/public/img/icons/access.png 14w,
					./src/public/img/icons/access-big.png 36w
					" sizes="
					(max-width: 980px) 36px,
					14px
					" src="./src/public/img/icons/access.png" alt="Accès au parc" class="access__icon" />
				Accès au&nbsp;parc
			</a>
		</li>

		<li class="access__item">
			<a href="#" class="access__link">
				<img srcset="
					./src/public/img/icons/plan.png 9w,
					./src/public/img/icons/plan-big.png 24w
					" sizes="
					(max-width: 980px) 24.2px,
					9px
					" src="./src/public/img/icons/plan.png" alt="Plan du parc" class="access__icon" />
				Plan du&nbsp;parc
			</a>
		</li>
	</ul>
</div>