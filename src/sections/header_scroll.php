<header class="header header__sticky" id="stickyHeader">
	<div class="container">
		<nav class="header__navigation header__desktop" role="navigation">
			<a href="#" class="header__logo-container">
				<img srcset="
						./src/public/img/logos/logo-x05.png 115w,
						./src/public/img/logos/logo.png 230w,
						./src/public/img/logos/logo-x2.png 460w" sizes="
						(max-width:465px) 130px,
						(max-width: 876px) 130px,
						(max-width:1080px) 160px,
						230px,
						" src="./src/public/img/logos/logo.png" alt="Logo du parc Astérix" class="header__logo" />
			</a>

			<ul class="header__nav-list">
				<li class="header__nav-item">
					<a href="#" class="header__nav-link">Le parc</a>
				</li>
				<li class="header__nav-item">
					<a href="#" class="header__nav-link">Les hôtels</a>
				</li>
				<li class="header__nav-item">
					<a href="#" class="header__nav-link">Les activités</a>
				</li>
				<li class="header__nav-item">
					<a href="#" class="header__nav-link">Le monde d'Asterix</a>
				</li>

				<li class="header__nav-item">
					<a href="#" class="btn btn-default">Préparer ma visite</a>
				</li>
				<li class="header__nav-item">
					<a href="#" class="btn btn-primary">Nos offres</a>
				</li>
			</ul>


		</nav>

		<nav class="header__navigation header__mobile" role="navigation">

			<div class="header__mobile-left">
				<button class="header__mobile-icon btn burger">
					<img class="icon" src="./src/public/img/icons/burger-black.png" />
				</button>


				<a href="#" class="btn btn-primary">Réserver</a>
			</div>

			<div class="header__mobile-right">
				<button class="header__mobile-icon btn search">
					<img class="icon" src="./src/public/img/icons/loupe-black.png" />
				</button>

				<button class="header__mobile-icon btn ticket">
					<img class="icon" src="./src/public/img/icons/tickets-black.png" />
				</button>

				<a class="header__mobile-icon btn user">
					<img class="icon" src="./src/public/img/icons/icn_compte-black.png" />
				</a>
			</div>

		</nav>
	</div>
</header>