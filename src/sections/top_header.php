<section class="top-header">
	<div class="container">
		<div class="top-header__container">
			<div class="top-header__week-container">
				<button class="btn btn-secondary">
					Cette semaine au parc <img class="icon" src="./src/public/img/icons/chevron-down-white.png" alt="Cette semaine au parc" />
				</button>

				<div class="top-header__info-content">
					<span class="top-header__status" data-status="open"></span>
					<p class="top-header__text">Ouvert<span class="top-header__text-detail"> de 10h à 18h</span></p>
					<span class="top-header__weather"><img class="icon" src="./src/public/img/icons/weather.svg" alt="état de la météo au Parc Astérix" /></span>
				</div>
			</div>

			<div class="top-header__user-container u-hide-below-tablet-lg">
				<span class="top-header__user-item search">
					<img class="icon" src="./src/public/img/icons/loupe.png" />
				</span>

				<span class="top-header__user-item lang">
					<span class="lang-text">FR</span>
					<img class="icon" src="./src/public/img/icons/chevron-down-white.png" />
				</span>

				<a class="top-header__user-item user">
					<img class="icon" src="./src/public/img/icons/icn_compte.png" />
				</a>
			</div>
		</div>
	</div>
</section>