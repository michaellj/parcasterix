<?php
require('./src/datas/pages.php');
$page = $pages['home'];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<?php require('src/partials/head.php'); ?>
</head>

<body>
	<?php require('src/sections/header_scroll.php'); ?>
	<?php require('src/templates/home.php'); ?>
	<?php require('src/partials/foot.php'); ?>
</body>

</html>